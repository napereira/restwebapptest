# README #

This is the solution for the requested exercise during the "Introduction to RESTful Web Services" session ([See Slides](http://www.dei.isep.ipp.pt/~npereira/REST.pdf); this includes the functionalities requested in Slide 43).

**It also shows how to perform tests on the RESTful service using JUnit annotations.**

### Who do I talk to? ###

* Nuno Pereira (nap@isep.ipp.pt)