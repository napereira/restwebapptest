package pt.dei.webapp;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/customer")
public class CustomerResource {
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Customer getCustomer() {
        //just return a new customer for testing...
        return new Customer("Billing Street, no. 1", "Shipping Square, no. 22");
    }
}
