package pt.dei.webapp;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "customer")
public class Customer {
    @XmlElement(name = "billing-address", required = true)
    private String billingAddress;
    @XmlElement(name = "shipping-address", required = true)
    private String shippingAddress;
    public Customer(){

    }

    public Customer(String bAddr, String sAddr) {
        billingAddress = bAddr;
        shippingAddress = sAddr;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Customer)) {
            return false;
        }

        Customer that = (Customer) other;

        // two customers are considered the same if all fields match
        return this.billingAddress.equals(that.billingAddress)
                && this.shippingAddress.equals(that.shippingAddress);
    }
}
