package pt.dei.webapp;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloResource {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String sayHello() {
        return "Hello World PLAIN text";
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public String sayXMLHello() {
        return "<?xml version='1.0'?><hello> Hello World XML</hello>";
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String sayHtmlHello() {
        return "<html><title>Hello World HTML</title><body><h1>Hello World HTML</h1></body></html>";
    }

    @POST
    @Consumes({"text/plain"})
    @Produces(MediaType.TEXT_PLAIN)
    public String create(@FormParam("param1") String param1,
                       @FormParam("param2") String param2) {
        return "Received param1:"+param1+"\nReceived param2:"+param2;
    }
}

