/**
 * A very simple example of performing tests on a REST API using
 * JUnit annotations
 *
 * @author      Nuno Pereira
 */

package pt.dei.webapp;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class MyResourceTest {

    private static final int PORT = 8080;
    private static final String BASE_URI = "http://localhost:8080/myapi/";

    private static Server server = new Server(PORT);
    private static Client client = Client.create();

    public void MyResourceTest() {};

    /**
     * Creates an instance of the web server (Jetty)
     *
     * @BeforeClass indicates that this will run before the tests start
     */
    @BeforeClass
    public static void startServer() throws Exception {
        WebAppContext context = new WebAppContext();
        context.setDescriptor("src/main/webapp/WEB-INF/web.xml");
        context.setResourceBase("src/main/webapp");
        context.setContextPath("/");
        context.setParentLoaderPriority(true);
        server.setHandler(context);
        server.start();
    }

    /**
     * Test /hello resource; show different ways to get responses
     *
     */
    @Test
    public void testGetHello() throws JSONException,
            URISyntaxException {
        WebResource webResource = client
                .resource(BASE_URI);

        // get the response into a String
        String helloText = webResource.path("/hello")
                .accept(MediaType.TEXT_PLAIN)
                .get(String.class);

        // get the response into a String (if we had a class that
        // represents this response, we could get and instance directly
        // from the response; see testGetCustomerObject() example below)
        String helloXML = webResource.path("/hello")
                .accept(MediaType.TEXT_XML)
                .get(String.class);

        // we can also get a ClienResponse instance...
        ClientResponse response = webResource.path("/hello")
                .accept(MediaType.TEXT_HTML)
                .get(ClientResponse.class);

        // check the response status
        assertEquals(response.getStatus(), 200);

        // get the the response (the HTML) into a String
        String helloHTML = response.getEntity(String.class);

        assertEquals("Hello World PLAIN text", helloText);
        assertEquals("<?xml version='1.0'?><hello> Hello World XML</hello>", helloXML);
        assertEquals("<html><title>Hello World HTML</title><body><h1>Hello World HTML</h1></body></html>", helloHTML);
    }

    /**
     * Performs a POST and check if reply is as expected
     *
     */
    @Test
    public void testPostHello() throws JSONException,
            URISyntaxException {
        WebResource webResource = client
                .resource(BASE_URI);

        String postReplyText = webResource.path("/hello")
                .accept(MediaType.TEXT_PLAIN)
                .post(String.class, "param1=Hello&param2=World");

        assertEquals("Received param1:Hello\nReceived param2:World", postReplyText);
    }

    /**
     * Gets and checks the JSON that describes a customer
     *
     */
    @Test
    public void testGetCustomerJson() throws JSONException,
            URISyntaxException {
        WebResource webResource = client
                .resource(BASE_URI);

        // get a JSONObject instance from the response
        JSONObject json = webResource.path("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .get(JSONObject.class);

        assertEquals("Billing Street, no. 1", json.get("billing-address"));
        assertEquals("Shipping Square, no. 22", json.get("shipping-address"));
    }

    /**
     * Gets and checks a Customer instance
     *
     */
    @Test
    public void testGetCustomerObject() throws JSONException,
            URISyntaxException {
        WebResource webResource = client
                .resource(BASE_URI);

        // get a Customer instance from the response
        Customer customer = webResource.path("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .get(Customer.class);

        // uses the equals() method to check if two instances are of the same customer
        assertEquals(new Customer("Billing Street, no. 1", "Shipping Square, no. 22"), customer);
    }
    @AfterClass
    public static void stopServer() throws Exception {
        server.stop();
    }
}
