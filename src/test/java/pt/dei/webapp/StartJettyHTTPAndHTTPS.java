package pt.dei.webapp;

import java.io.File;
import java.io.FileNotFoundException;

import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;

/*
*  Creates a Jetty server with HTTP and HTTPS connectors
*  from http://download.eclipse.org/jetty/stable-9/xref/org/eclipse/jetty/embedded/ManyConnectors.html
* */
public class StartJettyHTTPAndHTTPS {

    private static final int HTTP_PORT = 8080;
    private static final int HTTPS_PORT = 8081;

    public static void main(String[] args) throws Exception {
        // Create a basic jetty server object without declaring the port.
        Server server = new Server();

        // Path to a keystore
        // Created with: keytool -genkey -alias restwebapp.dei.isep.ipp.pt -keyalg RSA -keystore keystore.jks -keysize 2048
        String jettyDistKeystore = "src/main/webapp/keystore.jks";
        String keystorePath = System.getProperty(
                "keystore.jks", jettyDistKeystore);
        File keystoreFile = new File(keystorePath);
        if (!keystoreFile.exists())
        {
            throw new FileNotFoundException(keystoreFile.getAbsolutePath());
        }

        // HTTP Configuration
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSecurePort(HTTPS_PORT);
        http_config.setOutputBufferSize(32768);

        // HTTP connector
        ServerConnector http = new ServerConnector(server,
                new HttpConnectionFactory(http_config));
        http.setPort(HTTP_PORT);
        http.setIdleTimeout(30000);

        // SSL Context Factory for HTTPS
        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setKeyStorePath(keystoreFile.getAbsolutePath());
        sslContextFactory.setKeyStorePassword("2X4CigoqE7sv");
        sslContextFactory.setKeyManagerPassword("2X4CigoqE7sv");

        // HTTPS Configuration
        HttpConfiguration https_config = new HttpConfiguration(http_config);
        SecureRequestCustomizer src = new SecureRequestCustomizer();
        //src. setStsMaxAge(2000);
        //src.setStsIncludeSubDomains(true);
        https_config.addCustomizer(src);

        // HTTPS connector
        ServerConnector https = new ServerConnector(server,
                new SslConnectionFactory(sslContextFactory,HttpVersion.HTTP_1_1.asString()),
                new HttpConnectionFactory(https_config));
        https.setPort(HTTPS_PORT);
        https.setIdleTimeout(500000);

        // Set the connectors
        server.setConnectors(new Connector[] { http, https });

        // Set handler
        WebAppContext context = new WebAppContext();
        context.setDescriptor("src/main/webapp/WEB-INF/web.xml");
        context.setResourceBase("src/main/webapp");
        context.setContextPath("/");
        context.setParentLoaderPriority(true);
        server.setHandler(context);

        server.start();
        server.join();
    }
}
